/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author top
 */
public class ConfigReaderTest {
    
    public ConfigReaderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of readConfig method, of class ConfigReader.
     */
    @Test
    public void testgetElementsCount() {
        ConfigReader instance = new ConfigReader();
        int expResult = 3;
        ConfigClass result = instance.readConfig();
        assertEquals(expResult, result.getElementsCount());

    }
    @Test
    public void testgetOperationType() {
        ConfigReader instance = new ConfigReader();
        String expResult = "CONCATSTRING";
        ConfigClass result = instance.readConfig();
        assertEquals(expResult, result.getOperationType());

    }
    
}
