/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import input.InputData;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author top
 */
public class ConcatStringTest {
    List<String> dataStr = new ArrayList<>();
    InputData inData = new InputData();
    
    public ConcatStringTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of summ method, of class ConcatString.
     */
    
    @Test
    public void testSummOneNumber() {
        dataStr.add("1");
        dataStr.add("sd");
        dataStr.add("a3");
        inData.setInputDataList(dataStr);
        ConcatString instance = new ConcatString();
        String expResult = "1;sd;a3";        
        Result result = instance.summ(inData);
        assertEquals(expResult, result.getSumResult());        
    }
    @Test
    public void testSummNoNumber() {
        dataStr.add("s1");
        dataStr.add("sd");
        dataStr.add("a3");
        inData.setInputDataList(dataStr);
        ConcatString instance = new ConcatString();
        String expResult = "s1;sd;a3";        
        Result result = instance.summ(inData);
        assertEquals(expResult, result.getSumResult());
        
    }
    @Test
    public void testSummOnlyNumber() {
        dataStr.add("3");
        dataStr.add("4");
        dataStr.add("45");
        inData.setInputDataList(dataStr);
        ConcatString instance = new ConcatString();
        String expResult = "3;4;45";        
        Result result = instance.summ(inData);
        assertEquals(expResult, result.getSumResult());
        
    }
    
}
