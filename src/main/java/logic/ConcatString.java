/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import input.InputData;

/**
 *
 * @author top
 */
public class ConcatString extends Summator implements Summable {

    @Override
    public Result summ(InputData inData) {
        Result result = new Result();        
        result.setResStatus(Status.OK);
        result.setSumResult(inData.toString());
        return result;
    }

}
