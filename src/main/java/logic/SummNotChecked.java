/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import config.ConfigReader;
import input.InputData;

/**
 *
 * @author top
 */
public class SummNotChecked extends Summator implements Summable{
  
    @Override
    public Result summ(InputData inData) {
        Result result = new Result();
        int elementsCount = new ConfigReader().readConfig().getElementsCount();
        int summ = 0;
        //int counter = 0;
        for (int i = 0; i < elementsCount; i++) {
            if ((!(inData.getElement(i).isEmpty())) && (isNumeric(inData.getElement(i)))) {
                summ = summ + Integer.parseInt(inData.getElement(i));
                //counter++;
            }
        }        
            result.setResStatus(Status.OK);
            result.setSumResult(summ+"");        
        return result;
    }

}
