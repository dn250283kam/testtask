/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

/**
 *
 * @author top
 */
public class Result {
    private Status resStatus;   
    private String sumResult;
    

    public Status getResStatus() {
        return resStatus;
    }

    public void setResStatus(Status resStatus) {
        this.resStatus = resStatus;
    }

    public String getSumResult() {
        return sumResult;
    }

    public void setSumResult(String sumResult) {
        this.sumResult = sumResult;
    }        
}
