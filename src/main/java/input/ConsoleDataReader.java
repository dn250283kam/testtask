/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package input;

import config.ConfigReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author top
 */
public class ConsoleDataReader implements InputReadable {

    private static Logger logger = Logger.getLogger(ConsoleDataReader.class.getName());
    private BufferedReader bufReader;    
    
    @Override
    public InputData readData(InputStream is) {
        InputData dataStore = new InputData();
        bufReader = new BufferedReader(new InputStreamReader(is));
        try {
            int elementsCount = new ConfigReader().readConfig().getElementsCount();
            int cycleCounter = 0;
            boolean flag = true;
            while (flag) {
                if (cycleCounter < elementsCount) {
                    System.out.print("-> ");
                    dataStore.addElement(bufReader.readLine());                   
                } else {
                    flag = false;
                }
                cycleCounter++;
            }
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        } finally {
            close();
        }
        return dataStore;
    }

    private void close() {
        try {
            if (bufReader != null) {
                bufReader.close();
            }
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        }

    }

}
