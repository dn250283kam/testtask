/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package input;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author top
 */
public class InputData {
    private final char DELIMETER = ';';
    private List<String> inputDataList = new ArrayList<>();

    public List<String> getInputDataList() {
        return inputDataList;
    }

    public void setInputDataList(List<String> inputDataList) {
        this.inputDataList = inputDataList;
    }

    public void addElement(String element) {
        inputDataList.add(element);
    }

    public String getElement(int index) {
        return inputDataList.get(index);
    }

    public int size() {
        return inputDataList.size();
    }

    @Override
    public String toString() {
        String res = "";
        int i = 0;
        for (String inputDataListEl : inputDataList) {
            res = res + inputDataListEl;
            if (i < inputDataList.size() - 1) {
                res = res + DELIMETER;
            }
            i++;
        }
        return res;
    }
}
