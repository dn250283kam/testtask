package test;

import config.ConfigReader;
import config.OperationType;
import input.*;
import java.io.IOException;
import java.util.logging.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import logic.ConcatString;
import logic.Result;
import logic.SummChecked;
import logic.SummNotChecked;
import logic.Summable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author top
 */
public class Runer {

    private static final Logger logger = LoggerFactory.getLogger(ConsoleDataReader.class.getName());

    public static void main(String[] str) {
        try {
            LogManager.getLogManager().readConfiguration(
                    Runer.class.getResourceAsStream("/logging.properties"));
        } catch (IOException e) {
            System.err.println("Could not setup logger configuration: " + e.toString());
        }
        Runer runer = new Runer();
        runer.run();

    }

    public void run() {

        Summable sumRes;
        String operation = new ConfigReader().readConfig().getOperationType();
        try {
            switch (OperationType.valueOf(operation)) {
                case SUMMNOTCHECKED: {
                    sumRes = new SummNotChecked();
                    act(sumRes, operation);
                }
                break;
                case SUMMCHECKED: {
                    sumRes = new SummChecked();
                    act(sumRes, operation);
                }
                break;
                case CONCATSTRING: {
                    sumRes = new ConcatString();
                    act(sumRes, operation);
                }
                break;
            }
        } catch (IllegalArgumentException ex) {
            logger.error("unknown operation " + operation);
        }
    }

    private void act(Summable sumRes, String operation) {
        InputReadable input = new ConsoleDataReader();
        logger.info("operation is  " + operation);
        InputData inData = input.readData(System.in);
        logger.info("input data " + inData.toString());
        long timer = System.nanoTime();
        Result result = sumRes.summ(inData);
        System.out.println("-> result is " + String.valueOf(result.getSumResult()));
        logger.info("time is " + String.valueOf(System.nanoTime() - timer));
        logger.info("summ is " + String.valueOf(result.getSumResult()) + " status is " + result.getResStatus().toString());

    }
}
