/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author top
 */
public class ConfigReader implements Configable {

    private static Logger logger = Logger.getLogger(ConfigReader.class.getName());
    private Properties props = new Properties();

    public ConfigReader() {
        assign();
    }

    private void assign() {
        try {            
            BufferedReader propsIn = new BufferedReader(new FileReader("summator.cfg"));
            props.load(propsIn);
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public ConfigClass readConfig() {        
        ConfigClass cfgStore = new ConfigClass();
        cfgStore.setElementsCount(Integer.parseInt(props.getProperty("elementsCount")));
        cfgStore.setOperationType(props.getProperty("operationType"));
        return cfgStore;
    }

}
